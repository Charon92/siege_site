<?php

// Empty tips array
$tips = [];

// Adding tips to the array
$tips[001] = [
  "defence" => [
    "bomb" => [
      "type" => "bomb",
      "locations" => [
        "CEO and Executive Office" => [
          "title" => "CEO and Executive Office",
          "hiding_spots" => [
            1 => "Behind CEO desk",
            2 => "In Janitors Closet",
            3 => "In Conference Room",
            4 => "Behind Front desk",
            5 => "Elevators",
          ],
        ],
        "Open Area and Staff Room" => [
          "title" => "Open Area and Staff Room",
          "hiding_spots" => [
            1 => "Admin Office",
            2 => "Behind Staff Room counter",
            3 => "Stock Trading room (upstairs)",
            4 => "Janitors Closet",
            5 => "Elevators",
            6 => "Tellers",
            7 => "Archives",
          ],
        ],
        "Tellers Office and Archives" => [
          "title" => "Tellers Office and Archives",
          "hiding_spots" => [
            1 => "Behind shelving units",
            2 => "Admin Office",
            3 => "Behind Tellers desk",
            4 => "CEO Office",
            5 => "Staff Room",
          ],
        ],
        "CCTV and Lockers" => [
          "title" => "CCTV and Lockers",
          "hiding_spots" => [
            1 => "Secure Hallway",
            2 => "Behind servers",
            3 => "In Lockers",
            4 => "In Vault Lobby",
            5 => "In Garage",
          ],
        ],
      ],
    ],
    "hostage" => [

    ],
    "secure" => [

    ],
  ],
  "attack" => [
    "bomb" => [

    ],
    "hostage" => [

    ],
    "secure" => [

    ],
  ],
];
